
direction-field-app

Running at http://www.maths.usyd.edu.au/u/jburke/direction-field/index.html

To compile this yourself, clone the repo, and in that folder, run "npm install" and then "npm start". This will open a local server at http://localhost:3000/ where you will see the app.