import React from 'react';
import ReactDOM from 'react-dom';

import {unit_vector} from './matrix.js';

const ssv_color = '#ef8261';
const vec_color = '#934940';
const pt_color = '#d24749';

const axes_sep_const = .03;
const vec_sep_const = .6; // equal to vec length/delta


function relativeCoords ( event ) {
    var bounds = event.target.getBoundingClientRect();
    var x = event.clientX - bounds.left;
    var y = event.clientY - bounds.top;
    return ([x,y]);
}


function Plane(props) {
    const canvasRef = React.useRef(null);
    const [point, set_point] = React.useState(null);

    const vec_length = props.prop_obj.vf_delta*vec_sep_const;

    // data below could be passed in as props
    const grid_placement_const = .9;
    const init_coord_data = {
	canvas_xSize:650,
	canvas_ySize: 650,
	xMin: -grid_placement_const,
	xMax: 10.5 + grid_placement_const,
	yMin: -grid_placement_const,
	yMax: 10.5,
    };

    const coord = coord_factory(init_coord_data);   

    // this is the drawing loop; is called on each update
    React.useEffect( () => {
	const ctx = canvasRef.current.getContext('2d');
	ctx.font = "20px Arial";
	ctx.clearRect(0, 0, coord.get_xSize(), coord.get_ySize());

	// drawing the axes
	ctx.strokeStyle = '#000';
	ctx.fillStyle = '#000';
	ctx.lineWidth   = 1;
	ctx.globalAlpha=.7;
	draw_axes(ctx, coord);
	draw_grid(ctx, coord);
	ctx.globalAlpha=1;

	// if a point has been selected, draw the integral curve
	if(point) {	    
	    draw_integral_curve(props.prop_obj, ctx, coord, point);
	}

	ctx.globalAlpha=.5;
	draw_direction_field(props.prop_obj, ctx, coord);
	ctx.globalAlpha=1;
    });

    function handle_click_func(e) {
	let x = e.clientX;
	let y = e.clientY;

	set_point(relativeCoords(e));
    }

    function draw_integral_curve(prop_obj, ctx, crd, pt) {
	let delta = (prop_obj.ic_delta);
	let plane_delta = delta/(crd.get_horiz_ratio());
	let slope_func = prop_obj.slope_func.func;

	// radius of points in pixels
	const point_rad = 7;

	//
	// right of the point
	//
	// first done in plane coords
	let right_crds = [crd.from_canvas(pt)];
	for(let i = 1; i < Math.floor((crd.get_xSize()-pt[0])/delta-1);
	    i++){
	    right_crds[i] = [];
	    right_crds[i][0] = right_crds[i-1][0]+plane_delta;
	    right_crds[i][1] = right_crds[i-1][1]+
		plane_delta*(slope_func(right_crds[i-1]));	    
	}

	let canvas_right_crds = right_crds.map( (v) => crd.to_canvas(v));

	canvas_right_crds.map((point, index) => {
	    canvas_draw_point(ctx,point,point_rad);
	    if(index > 0) {
		ctx.strokeStyle = '#000';
		ctx.fillStyle = '#000';
		ctx.lineWidth   = 2;
		canvas_line_seg(ctx, canvas_right_crds[index-1],
				point);
		ctx.strokeStyle = '#000';
		ctx.fillStyle = '#000';
		ctx.lineWidth   = 1;
	    }
	});

	//
	// left of the point
	//
	// first done in plane coords
	// this array is the reverse of what we want
	let left_crds_r = [crd.from_canvas(pt)];
	for(let i = 1; i < Math.floor(pt[0]/delta)-1; i++){
	    left_crds_r[-i] = [];
	    left_crds_r[-i][0] = left_crds_r[-i+1][0]-plane_delta;
	    left_crds_r[-i][1] = left_crds_r[-i+1][1]-
		plane_delta*(slope_func(left_crds_r[-i+1]));	    
	}

	let left_crds = [];
	for(let i = 0; i <  Math.floor(pt[0]/delta)-1; i++) {
	    left_crds[i] = left_crds_r[-i];
	}
	
	let canvas_left_crds = left_crds.map( (v) => crd.to_canvas(v));

	canvas_left_crds.map((point, index) => {
	    canvas_draw_point(ctx,point,point_rad);
	    if(index > 0) {
		ctx.strokeStyle = '#000';
		ctx.fillStyle = '#000';
		ctx.lineWidth   = 2;
		canvas_line_seg(ctx, canvas_left_crds[index-1],
				point);
		ctx.strokeStyle = '#000';
		ctx.fillStyle = '#000';
		ctx.lineWidth   = 1;
	    }
	});
	

    }
    
    function draw_direction_field(prop_obj, ctx, crd) {
	const delta = prop_obj.vf_delta;
	const xSize = crd.get_xSize();
	const ySize = crd.get_ySize();

	const slope_func = prop_obj.slope_func.func;

	let u;
	let grad;
	
	for(let i = 1; i < Math.floor(ySize/delta); i++) {
	    for(let j = 0; j < Math.floor(xSize/delta); j++) {
		u = unit_vector([1, -slope_func(
		    crd.from_canvas([i*delta,j*delta]))]);
		
		// grad = ctx.createLinearGradient(i*delta, j*delta,
		// 				i*delta+vec_length*u[0], j*delta+vec_length*u[1]);
		// grad.addColorStop(0,"#134693");
		// grad.addColorStop(1,"#eb1276");

		ctx.lineWidth = 2;
		canvas_line_seg(ctx, [i*delta,j*delta],
				[i*delta+vec_length*u[0], j*delta+vec_length*u[1]],); //grad);
		ctx.lineWidth = 1;
	    }
	}
    }

    
    function draw_axes(ctx, crd, ticks = 4, ) {
	let bds = crd.get_bounds();	
	const tick_size = 5;
	
	let left_point = crd.to_canvas([bds.xMin,0]);
	let right_point = crd.to_canvas([bds.xMax,0]);
	let bot_point = crd.to_canvas([0,bds.yMin]);
	let top_point = crd.to_canvas([0,bds.yMax]);
	let origin = crd.to_canvas([0,0]);

	//----------------------------------------
	// pulls the axes away from the edges of the container	
	//----------------------------------------	
	const xdel = (right_point[0]-left_point[0])*axes_sep_const;
	const ydel = (bot_point[1]-top_point[1])*axes_sep_const;
	
	left_point = [left_point[0]+xdel,left_point[1]];
	right_point = [right_point[0]-xdel,right_point[1]];
	top_point = [top_point[0], top_point[1]+ydel];
	bot_point = [bot_point[0], bot_point[1]-ydel];
	//----------------------------------------
		
	canvas_vector(ctx, bot_point, top_point);	
	canvas_vector(ctx, left_point, right_point);
	ctx.fillText("x",right_point[0]-10,right_point[1]+25);
	ctx.fillText("y",top_point[0]-25,top_point[1]+10);

	//----------------------------------------
	//ticks
	//----------------------------------------
	// y axis
	let ten_point = crd.to_canvas([0,10]);
	let lp = [ten_point[0]-tick_size, ten_point[1]];
	let rp = [ten_point[0]+tick_size, ten_point[1]];
	canvas_line_seg(ctx,lp,rp);
	ctx.fillText("10", lp[0]-25, lp[1]+10);

	// x axis
	ten_point = crd.to_canvas([10,0]);
	let tp = [ten_point[0],ten_point[1]-tick_size];
	let bp = [ten_point[0], ten_point[1]+tick_size];
	canvas_line_seg(ctx,tp,bp);
	ctx.fillText("10", bp[0]-10, bp[1]+25);
	
    }

    function draw_grid(ctx) {

    }

    // draws vector from start to end
    function canvas_vector(ctx,  start, end) {		
	ctx.beginPath();
	ctx.moveTo(start[0],start[1]);
	ctx.lineTo(end[0],end[1]);		
	ctx.stroke();
	ctx.fill();
	ctx.closePath();
	
	canvas_arrow(ctx, start[0], start[1], end[0], end[1],10);		
    }

    function canvas_line_seg(ctx,  start, end, stroke_style='color') {
	ctx.strokeStyle = stroke_style;
	ctx.beginPath();
	ctx.moveTo(start[0],start[1]);
	ctx.lineTo(end[0],end[1]);		
	ctx.stroke();
	ctx.fill();
	ctx.closePath();
    }

    // taken from https://stackoverflow.com/questions/808826/draw-arrow-on-canvas-tag
    function canvas_arrow(context, fromx, fromy, tox, toy, r=10){
	let x_center = tox;
	let y_center = toy;

	let angle;
	let x;
	let y;

	context.beginPath();
	angle = Math.atan2(toy-fromy,tox-fromx);
	x = r*Math.cos(angle) + x_center;
	y = r*Math.sin(angle) + y_center;

	context.moveTo(x, y);

	angle += (1/3)*(2*Math.PI);
	x = r*Math.cos(angle) + x_center;
	y = r*Math.sin(angle) + y_center;

	context.lineTo(x, y);

	angle += (1/3)*(2*Math.PI);
	x = r*Math.cos(angle) + x_center;
	y = r*Math.sin(angle) + y_center;

	context.lineTo(x, y);

	context.stroke();
	context.fill();
	context.closePath();
    }

     function canvas_draw_point(ctx, [x,y], rad=5) {
	ctx.beginPath();
	ctx.arc(x,y,rad,0,2*Math.PI);	
	ctx.fill();
	ctx.closePath();
    }
    
    function draw_point(ctx, [x,y], rad=5) {
	const center = coord.to_canvas([x,y]);
	
	ctx.beginPath();
	ctx.arc(center[0],center[1],rad,0,2*Math.PI);	
	ctx.fill();
	ctx.closePath();
    }

    return <canvas
    ref={canvasRef}
    width={coord.get_xSize()}
    height={coord.get_ySize()}
    onClick = {handle_click_func}
	/>;
}

//example input:
// 
// const grid_placement_const = .09;
// const init_coord_data = {
//     canvas_xSize:550,
//     canvas_ySize: 550,
//     xMin: -grid_placement_const,
//     xMax: 10 + grid_placement_const,
//     yMin: -grid_placement_const,
//     yMax: 10 + grid_placement_const,
// }

function coord_factory(init_data) {

    const xMin = init_data.xMin;
    const xMax = init_data.xMax;
    const yMin = init_data.yMin;
    const yMax = init_data.xMax;
    const canvas_xSize = init_data.canvas_xSize;
    const canvas_ySize = init_data.canvas_ySize;

    function to_canvas(pt) {	
	const xPerc = (pt[0]-xMin)/(xMax-xMin);
	const yPerc = (pt[1]-yMin)/(yMax-yMin);
	return ([(xPerc * canvas_xSize),
		 (canvas_ySize - yPerc * canvas_ySize)]);
    }

    function from_canvas(pt) {
	const xPerc = pt[0]/canvas_xSize;
	const yPerc = pt[1]/canvas_ySize;

	return ([xMin + xPerc*(xMax-xMin), yMin + (1-yPerc)*(yMax-yMin)]);
    }

    function get_bounds() {
	return{xMin:xMin, xMax:xMax, yMin:yMin, yMax:yMax};
    }

    function get_xSize() {
	return init_data.canvas_xSize;
    }

    function get_ySize() {
	return init_data.canvas_ySize;
    }

    function get_horiz_ratio() {
	return canvas_xSize/(xMax-xMin);
    }

    function get_vert_ratio() {
	return canvas_ySize/(yMax-yMin);
    }

    return {to_canvas, from_canvas, get_bounds, get_xSize, get_ySize,
	    get_horiz_ratio, get_vert_ratio};
}



export {Plane};
