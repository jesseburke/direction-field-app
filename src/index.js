import React from 'react';
import ReactDOM from 'react-dom';
// this is to display tex
import 'katex/dist/katex.min.css';
import TeX from '@matejmazur/react-katex';
// tabs
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";
// inline styles
import { Flex, Box } from '@rebass/grid';

import {Plane} from './plane.js';
import './index.css';


// function returns slope of tangent line at (x,y);
// delta is used to construct integral curves;
// vector spacing is the distance between in direction field;
// active input is which tab is active
let identity = ((pt) => 1);
const start_data = {func:identity,
		    vf_delta:15,
		    ic_delta:15,
		    vector_spacing:25,
		    active_input:0}; 

ReactDOM.render(<App data={start_data}/>,
		document.getElementById("main-div"));


//----------------------------------------
//
// expects:
//
//----------------------------------------

function App(props) {
    const [n_value, set_n_value] = React.useState(0);

    let obj = {func:props.data.func};
    // function returns slope of tangent line at (x,y);
    const [slope_func, set_slope_func] = React.useState(obj);
    const [vf_delta, set_vf_delta] = React.useState(props.data.vf_delta);
    const [ic_delta, set_ic_delta] = React.useState(props.data.ic_delta);
    const [vector_spacing, set_vector_spacing] = React.useState(props.data.vector_spacing);
    const [active_input, set_active_input] = React.useState(props.data.active_input);

    function handle_tab_select(tab_index) {
	set_active_input(tab_index);
    }

    const prop_obj = {slope_func,
		      vf_delta,
		      ic_delta,
		      vector_spacing,
		      active_input};
    
    return (<div className="main-div">
	    <Flex flexDirection="row" justifyContent="space-around">

	    <Box>
	    <Flex flexDirection="column" justifyContent="space-around"
	    alignItems="center">	        
	    
	    <Box>
	    <div className="input-comp">
	    <Tabs selectedIndex={active_input} onSelect={handle_tab_select}>
	    <TabList>
	    <Tab>Logistic</Tab>
	    <Tab>Flow</Tab>
	    <Tab>Polynomial</Tab>
	    <Tab>Second order</Tab>
	    </TabList>

	    <TabPanel>
	    <LogisticEq change_func={(f)=>(set_slope_func({func:f}))}/>
	    </TabPanel>

	    <TabPanel>
	    <FlowEq change_func={(f)=>(set_slope_func({func:f}))}/>
	    </TabPanel>

	    <TabPanel>
	    <PolynomialEq change_func={(f)=>(set_slope_func({func:f}))}/>
	    </TabPanel>

	    <TabPanel>
	    <SecondOrderEq/>
	    </TabPanel>
	    </Tabs>
	    </div>
	    </Box>
	 
	    </Flex>
	    </Box>
	    
	    <Box width="650px">
	    <div className="math-plane">
	    <Plane prop_obj={prop_obj}/>
	    </div>	    
	    </Box>
	    
	    </Flex>
	    </div>
	   );
}

function PlaneBounds(props) {
    return (<div className="input-comp">
	    <table><tbody>
	    <tr><td></td><td>ymax</td><td></td></tr>
	    <tr><td>xmin</td><td></td><td>xmax</td></tr>
	    <tr><td></td><td>ymin</td><td></td></tr>
	    </tbody>
	    </table>
	    </div>);
}

function LogisticEq(props) {
    const [a_value, set_a_value] = React.useState(0);
    const [k_value, set_k_value] = React.useState(0);

    // index = 0 if k changed, and 1 if a changed
    function handle_change_func(val,index) {
	if(index === 0) {	    
	    set_k_value(val);
	}
	else {
	    set_a_value(val);
	}
	
	props.change_func(tangent_slope);
    }

    function tangent_slope(pt) {
	const x = pt[0];
	const y = pt[1];

	return Number(k_value)*y - Number(a_value)*(y**2);
    }

    	// <Box>
	// <Flex flexDirection ="row" justifyContent="center">
	// <TeX>{String.raw`\frac{dy}{dx}=k\cdot y - a \cdot y^2`}</TeX>
	// </Flex>
	// </Box>	
    
    return <Box width="500px">
	<Flex flexDirection="column" justifyContent="center">

	<Box>
	
	<Flex flexDirection="row" justifyContent="space-around" alignItems="center">

	<Box>
	<Slider value={k_value} step=".1"
    on_change_f={(val)=>handle_change_func(val,0)}
	label="k" min="0" max="10"/>
	<Slider value={a_value} step=".1"
    on_change_f={(val)=>handle_change_func(val,1)}
    label="a" min="0" max="10"/>
	</Box>

	<Box>
	
	<Flex flexDirection="column" justifyContent="center">
	<Box>
	<TeX>
    {String.raw`\frac{dy}{dx}=${round(k_value)}\cdot y - (${round(a_value)}) \cdot y^2`}
    </TeX>
	</Box>
	</Flex>
	
	</Box>

        </Flex>
	
	</Box>
	</Flex>
	</Box>;
}

function FlowEq(props) {   
    const [k_value, set_k_value] = React.useState(0);

     function handle_change_func(val) {

	 set_k_value(val);
	 props.change_func(tangent_slope);
    }

    function tangent_slope(pt, round) {
	const x = pt[0];
	const y = pt[1];

	return Number(k_value)*Math.sqrt(y); 
    }
    
    return <Box width="500px">
	<Flex flexDirection="column" justifyContent="center">

	<Box>
	
	<Flex flexDirection="row" justifyContent="space-around" alignItems="center">

	<Box>
	<Slider value={k_value} step=".1"
    on_change_f={handle_change_func} min="-10" max="0"
	label="k"/>
	</Box>

	<Box>
	
	<Flex flexDirection="column" justifyContent="center">
	<Box>
	<TeX>
    {String.raw`\frac{dy}{dx}=${round(k_value)}\sqrt{y}`}
    </TeX>
	</Box>
	</Flex>
	
	</Box>

        </Flex>
	
	</Box>
	</Flex>
	</Box>;
}

function PolynomialEq(props) {
    const [a_value, set_a_value] = React.useState(0);
    const [b_value, set_b_value] = React.useState(0);
    const [c_value, set_c_value] = React.useState(0);

    function handle_change_func(val, index) {
	if( index === 0) {
	    set_a_value(val);
	}
	else if (index === 1) {
	    set_b_value(val);
	}
	else {
	    set_c_value(val);
	}
	 props.change_func(tangent_slope);
    }

    function tangent_slope(pt) {
	const x = pt[0];
	const y = pt[1];

	return (Number(a_value)*x -Number(b_value)*(x**2) - Number(c_value)*(x**3));
    }
    
    return <Box width="500px">
		<Flex flexDirection="column" justifyContent="center">

	<Box>	
	<Flex flexDirection="row" justifyContent="space-around" alignItems="center">

	<Box>
	<Slider value={a_value} step=".1"
    on_change_f={(val)=>handle_change_func(val,0)}
    label="a" min="-10" max="10"/>
		<Slider value={b_value} step=".1"
    on_change_f={(val)=>handle_change_func(val,1)}
	label="b" min="-10" max="10"/>
	<Slider value={c_value} step=".1"
    on_change_f={(val)=>handle_change_func(val,2)}
    label="c" min="-10" max="10"/>
	</Box>

	<Box>
	
	<Flex flexDirection="column" justifyContent="center">
	<Box>
	<TeX>
	{String.raw`\frac{dy}{dx}=${round(a_value)}\cdot x 
             - (${round(b_value)}) \cdot x^2 
             - (${round(c_value)}) \cdot x^3`}
        </TeX>
	</Box>
	</Flex>
	
	</Box>

        </Flex>
	</Box>

        </Flex>
	</Box>;
}

function SecondOrderEq(props) {   
    const [k_value, set_k_value] = React.useState(0);    
    
    return <Box width="500px">
	<Box>
	Coming soon!
	</Box>
	</Box>;
}

//----------------------------------------
//
// expects:
// props.n_value, props.on_change_f
//
//----------------------------------------

function Slider(props) {
    const style = {width:150, margin:30};

    return (<div style={style}>
	    <input name="n" type="range" value={props.value} step={props.step}
	    onChange={(e) => props.on_change_f(e.target.value)}
	    min={props.min} max={props.max}
	    />
	    <label htmlFor="range_n">{props.label}={round(props.value)}</label>
	    </div>);
}

// not sure about this
function round(num,n=6) {
    return (Math.round((10**n)*num+.00001)/10**n);
}

export default App;
